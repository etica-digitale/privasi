---
#Title: "uBlock Origin"
weight: 22

_build:
  render: never
---

# uBlock Origin

> Quando navighiamo su un sito internet, la maggior parte delle volte il nostro browser comunica con tantissimi altri servizi internet che non sono il sito in questione, molte volte per ragioni lecite, altre volte invece è solo un pretesto per tracciarci. In questo capitolo impareremo a difenderci sbarazzandoci di tutte queste inutili connessioni. 

# In parole semplici

Il modo migliore per comprendere il problema è vederlo con i propri occhi.

Navighiamo su un sito qualunque, ad esempio il sito de [la Repubblica](https://www.repubblica.it) e clicchiamo sull'icona di uBlock Origin in alto a destra. Comparsa la finestrella, clicchiamo in basso su `Altro` finché non viene mostrata una sezione aggiuntiva a sinistra. In quella sezione verranno mostrati tutti i siti ai quali il nostro browser si è collegato mentre abbiamo aperto il sito:

![L3-3_gif0.gif](../../images/L3-3_gif0.gif)

Ci siamo collegati al sito de la Repubblica, e uBlock Origin ci mostra svariati altri siti e servizi diversi a cui il nostro browser si è collegato.

Ciascuna di queste "terze parti" ora sa che abbiamo navigato sul sito in questione, e fatto una determinata operazione (letto un articolo, guardato un video, e così via). E molto probabilmente nel frattempo ci hanno tracciati e profilati (a questo punto della guida non dovrebbe più stupirci).

Il web nel corso degli anni si è complicato così tanto che moltissimi siti per funzionare hanno bisogno dell'aiuto di terze parti per fornire pezzi di sito, e alcuni di questi possono interessarci come video, immagini, o parti varie. Agiremo dunque con una vera e propria operazione di potatura, dove taglieremo via i rami secchi che non vogliamo e invece manterremo quelli più sani e necessari alla sopravvivenza dell'albero.


# Cosa fare

> DA FARE: suddividere in livello standard (questo che segue) e un livello avanzato, con i cookie in modo chirurgico

Cliccate di nuovo sull'icona di uBlock Origin, e come avevamo fatto nel [capitolo di Firefox](../../percorso/l2-2___firefox/) premiamo sulla rotella in basso per tornare dentro la `Dashboard`. Recatevi nel pannello `Opzioni`, e finalmente spuntiamo la casella `Sono un utente avanzato` per abilitare le funzionalità che fanno al caso nostro.

![L3-3_gif1.gif](../../images/L3-3_gif1.gif)

A questo punto clicchiamo di nuovo l'icona di uBlock Origin, e come nella GIF premiamo le due caselline che diventeranno rosse e poi sul lucchetto e sul refresh (le freccette in cerchio).

![L3-3_gif2.gif](../../images/L3-3_gif2.gif)

Abbiamo in poche parole bloccato l'accesso a tutte le terze parti (le caselline rosse) e impediro loro di usare JavaScript (un codice di programmazione che rende i siti più interattivi ma al tempo stesso più vulnerabili) e ad altre piccole funzioni.  

E se il sito non funziona come dovrebbe? Finché si tratta di qualche impostazione grafica, vi consigliamo di passarci sopra. Se invece a non funzionare sono cose necessarie alla navigazione e non masticate tecnologia potete risolvere in due semplici passaggi:
- dal sito in questione cliccate sull'iconcina di uBlock Origin e riabilitate gli script di terze parti cliccando 1) *a sinistra* della casellina rossa più in alto che diventerà grigia 2) sul lucchetto per ricordare le preferenze 3) sul refresh
- se continuasse a dar problemi, riaprite l'iconcina e cliccate *a sinistra* della casellina rossa *opaca* più in basso, che diventerà grigia. Di nuovo lucchetto e refresh

![L3-3_gif3.gif](../../images/L3-3_gif3.gif)

Così facendo avete riportato uBlock Origin come prima di leggere questo capitolo, ma solo su quel sito. In alcuni siti non succede nulla, in altri sì, ma meglio farsi male da una parte che da tutte le parti. Per esempio, GitLab (questo sito) richiederà il primo passaggio per leggere i capitoli, ma non manda comunque richieste a siti esterni.  

[Torna al percorso](../../indice)
