---
#Title: "Cookie: briciole di internet"
weight: 11

---

# Cookie: briciole di internet

Come Pollicino seminava briciole per non perdere la strada[^1], quando navigano su internet le persone seminano briciole per ricordare del proprio passaggio - con la differenza che su internet sono i siti a lasciarle per noi. Queste briciole sono i *cookie* (lett. "biscotto").  

I cookie sono un piccolo contenitore di informazioni che viene salvato *nel proprio browser* e che i siti usano per salvare delle informazioni riguardo la nostra persona: possono essere utili, per esempio, per ricordare la lingua scelta, gli oggetti nel carrello o l'accesso a un sito, così da non dover rifare tutto al riaprire internet. Al contrario, se usati in modo malevolo possono diventare uno strumento per tracciare gli spostamenti da un sito all'altro: basta che chi vuole tracciarci abbia visione anche sugli altri posti che visitiamo, mista a cattive intenzioni.  

Questa visione gli viene fornita dai siti stessi, che ne ospitano dei pezzi (le cosiddette "terze parti") generalmente in cambio di un servizio. Ci sono svariati motivi che spingono a tali scelte, come per esempio voler conoscere il traffico generato dal proprio sito e avere una remunerazione tramite annunci pubblicitari. In questo caso, prediligendo la soluzione più nota, è molto probabile si finirebbe a usare Google Analytics[^2]. Per avere un quadro generale, nel 2015 i siti che montavano Google Analytics erano stimati essere tra i 30 e i 50 milioni[^3], permettendo quindi al colosso tecnologico di invadere gli spazi delle persone ben oltre l'ecosistema dell'azienda (ricerche, posta ecc).  

È importante sottolineare che non è necessario essere registrati al sito tracciante per far sì che questo operi nelle retrovie: Mark Zuckerberg stesso ha ammesso davanti al Congresso statunitense che Facebook genera profili ombra dalle persone che non si sono mai registrate al social; per farlo incrocia varie informazioni, quali appunto i cookie lasciati in giro per la rete[^4].  

Fortunatamente, la legge europea viene in nostro soccorso con il Regolamento Generale sulla Protezione dei Dati (RGPD, o GDPR in inglese) che, tra le tante cose, obbliga i siti a mostrare l'avviso per accettare o rifiutare i cookie di profilazione[^5]. Un primo passo per negare il consenso quindi è sicuramente quello di rifiutare tutto quello che si può (perché tanto i cookie necessari al funzionamento andranno comunque), continuando al tempo stesso a rivendicare il proprio diritto all'intimità.  

Questo è un livello dedicato alle briciole, e a come lasciarne il meno possibile.  
  
[Torna al percorso](../../indice/)  

## Appendice
[^1]: https://it.wikipedia.org/wiki/Pollicino
[^2]: Ci sono invece soluzioni che, richiedendo un po' più di tempo, permettono di fare certe cose proteggendo la privacy di chi il sito lo visita. Una fra tutte è Matomo https://matomo.org/
[^3]: McGee Matt, [As Google Analytics Turns 10, We Ask: How Many Websites Use It?](https://marketingland.com/as-google-analytics-turns-10-we-ask-how-many-websites-use-it-151892), Maketing Land, 2015  
[^4]: Rossi Filippo, [Facebook: ecco come e perché raccoglie dati di chi non ha un account](https://www.business.it/facebook-raccoglie-dati-chi-non-ha-account/), Business.it, 2018
[^5]: Se il sito dispone solo di cookie tecnici non è necessario mostrarlo. Vedasi https://legalblink.it/post/cookie-policy-banner-cookie-quando-servono-o-sono-obbligatori.html
