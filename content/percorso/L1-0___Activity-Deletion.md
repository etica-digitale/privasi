---
weight: 2
---

# Rimozione consenso

>La prima modalità di Google per generare soldi è la pubblicità[^1]. Per aumentare le probabilità di vendita, l'azienda profila l'utenza per ottenere quante più informazioni possibili sul suo conto come dimostrabile [qui](https://myactivity.google.com/myactivity) (dovete essere connessi con il profilo Google) e [qui](https://www.google.com/maps/timeline). Tuttavia, disattivando determinate impostazioni, si potrà continuare ad usare il servizio, eliminando i dati già raccolti e annullando (o limitando, a discrezione dell'utente) la raccolta di quelli successivi. Il tutto senza futuri problemi di utilizzo.

## In parole semplici
Si immagini una mattina di entrare in una biblioteca privata. Si sta 10 minuti, si cerca un libro, lo si prende in prestito lasciando i propri dati, e se ne si va.
  
Si immagini invece ora che all'entrare nella biblioteca, un commesso fermi al'entrata, porgendo un questionario e dicendo: "per entrare va compilato questo foglio con tutti i suoi dati e accettare che le telecamere monitorino tutti i suoi movimenti, per migliorare la sua esperienza". Se si accetta, si entra. Se non si accetta, sapete dov'è l'uscita[^2].  
  
Raccogliere informazioni è una cosa naturale che ci permette di sopravvivere, instaurare rapporti e, in generale, muoverci per il mondo. Per esempio, se la mamma chiede al figlio cosa vuole mangiare, nessuno pensa che lo stia facendo per tenerlo sotto torchio. Al contrario, se gli mette una cimice nello zaino, è a dir poco preoccupante.  
Ottenere informazioni non è quindi di per sé sbagliato; è anzi vitale. Il problema è *come* le informazioni vengono ottenute, *quante* e *per quale scopo*. La differenza tra le due biblioteche è enorme.  
  
Google è la seconda biblioteca: quando create un profilo e accettate i termini e condizioni d'utilizzo, gli state dando ogni permesso possibile e immaginabile per profilarvi nel tempo (come le telecamere nella biblioteca) e il suo impero è così grande che dove ci si gira ci si gira, ci sono servizi Google. Salvo casi eccezionali questi dati (le vostre abitudini) restano a Google, tuttavia il problema rimane un altro: *perché* condividere con una macchina cose che, se sommate, non diremmo neanche alla persona più cara? Cosa significa "migliorare la tua esperienza"? Significa dare app gratuite in cambio di schedare continuamente noi e chi ci sta intorno? Si sta davvero migliorando l'esperienza o si sta piuttosto dando un contentino all'individuo, per giustificare l'erosione di diritti e sfera privata che permettono all'azienda (e a intere nazioni) di arricchirsi?
  
È come se l'ottico regalasse occhiali alla condizione di montarci sopra una microtelecamera dotata di microfono. Per registrare tutte le cose che si vedono, dicono, fanno e sentono. Nessuno sta dicendo di non prenderli, ma è dimostrato che chi sa di essere monitorato, inizia ad autocensurarsi[^3]. Chi cercherebbe su Google "inno ISIS" senza il timore che qualcuno o qualcosa dall'altra parte non stia pensando di aver di fronte un terrorista? O quante persone visitano i siti porno senza usare la modalità incognito (che proprio incognito non è[^4])?
  
Gli occhiali già ce li abbiamo e buttarli di punto in bianco in questo caso sarebbe sconsiderato (ricordiamoci che c'è gran parte della nostra vita, probabilmente anche lavorativa). Iniziamo dalla cosa più semplice allora: prendiamo gli occhiali, cancelliamo le vecchie registrazioni e mettiamo un pezzo di nastro adesivo sulla telecamera.


## Cosa fare
> ATTENZIONE: i procedimenti di questo capitolo saranno indicati solo per PC per non rendere il contenuto troppo lungo

### Scaricamento dati personali
Prima di tutto, richiediamo l'intera copia dei dati che Google ha sul nostro conto da qui: https://takeout.google.com/. Questo è molto importante perché ci aiuta a capire nel dettaglio quante cose sa Google, senza contare il fatto di avere una copia tutta nostra da conservare per bene. L'operazione potrebbe richiedere da alcuni minuti a un paio di giorni, a seconda di quanto e *da* quanto usate il profilo (in media qualche ora, e non dovete tenere il PC acceso); il nostro consiglio è di aspettare di avere la copia prima di continuare con il resto. Di darci una bella spulciata e poi di tornare qui.

### Lista attività
Una volta ottenuta la copia dei dati, iniziamo dall'eliminare la lista di attività dirigendoci [qua](https://myactivity.google.com/myactivity) e assicurandoci di essere connessi con il profilo Google (in caso contrario, ci avviserà e non vedremo nessuna lista attività). In alto a destra troviamo 3 puntini: clicchiamoci e nel menù a scomparsa selezioniamo "Elimina attività per"

![L1-0_gif0.gif](../../images/L1-0_gif0.gif)

Nella schermata seguente ci chiederà quali dati cancellare e da quando a quando. Sotto "Elimina per data" clicchiamo su "Oggi" e nel menù a scomparsa selezioniamo "Sempre" (ovvero "da sempre", tutti i dati). Al premere su "Elimina" il motore di ricerca ci chiederà più volte se siamo sicuri: confermiamo tutte le volte. *Poof*, attività cancellate.<br>

![L1-0_gif1.gif](../../images/L1-0_gif1.gif)


### Tracciamento GPS
Per eliminare i nostri spostamenti invece, andiamo [qui](https://www.google.com/maps/timeline). Per renderci conto del dettaglio di informazione, vi invitiamo a cliccare su qualsiasi puntino rosso nella cartina: sulla sinistra apparirà la cronologia completa di quell'evento. Non si esagera quando si parla di *ogni* movimento tracciato.  

In basso a destra della cartina troveremo l'icona di un cestino. Premiamola, spuntiamo "Ho capito e voglio eliminare tutta la Cronologia delle posizioni" e infine clicchiamo su "Elimina Cronologia delle posizioni". Se ci dovesse chiedere ulteriori conferme, confermiamo. Meno due.

![L1-0_gif2.gif](../../images/L1-0_gif2.gif)

### Disabilitazione future raccolte dati
Quello che abbiamo fatto per ora è stato eliminare i dati *già* raccolti. Il prossimo passo è impedire che ne raccolga di nuovi. Andiamo nella nostra [Gestione Attività](https://myaccount.google.com/activitycontrols) Google. Qua troveremo un totale di 6 impostazioni che possiamo disabilitare. Viene da sé che si consiglia la disabilitazione di tutte quante, ma la scelta è prima di tutto vostra. Segue un riassunto delle opzioni.
- **Attività Web e app**: siti visitati, app aperte, cronologia ricerche, da dove, come, quando e per quanto tempo. In sintesi, la lista attività
- **Cronologia delle posizioni**: salvataggio spostamenti quando abilitate il GPS (il tracciamento di prima).
- **Informazioni del dispositivo**: info quali i contatti che avete in rubrica, eventi nel calendario, app installate, specifiche dispositivo
- **Attività vocale e audio**: salvataggio traccia audio di tutto ciò che dite con "Ok, Google" o, comunque, interagendo con il microfono Google
- **Cronologia delle ricerche di YouTube**: salvataggio di ciò che cercate su YouTube
- **Cronologia visualizzazioni di YouTube**: salvataggio di ciò che guardate/avete guardato su YouTube

Per disabilitarle, basterà cliccare sul bottoncino blu sulla destra di ogni opzione, che diventerà grigio. Google definirà l'attività "in pausa", ovvero disabilitata.
Complimenti: in 10 minuti avete già arginato grandi quantità di dati, passati e futuri.

[Torna al percorso](../../indice)

## Appendice
[^1]: Entrate generate nel primo trimestre 2019: https://abc.xyz/investor/static/pdf/2019Q1_alphabet_earnings_release.pdf  
[^2]: Per dovere di cronaca, i "magheggi" di questo business non sono finiti qui (es. i cookie), ma parlarne in questa sezione sarebbe prematuro e creerebbe più confusione che altro.  
[^3]: [https://www.brennancenter.org/sites/default/files/publications/2019_DHS-SocialMediaMonitoring_FINAL.pdf](https://www.brennancenter.org/sites/default/files/publications/2019_DHS-SocialMediaMonitoring_FINAL.pdf)  
[^4]: Che per la cronaca, quello che fa è non lasciare tracce sul *vostro* computer. Chrome (e quindi Google) sa benissimo dove state andando. `¯\_(ツ)_/¯`  
