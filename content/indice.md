---
Title: Indice
---

> **Come usare la lista**: Chi va piano, va sano e va lontano. L'obiettivo non è arrivare primi (non c'è nessun premio), bensì capire *cosa* fa il singolo punto e il *perché* è stato suggerito. Cliccateci su e si apriranno le istruzioni precedute da una breve spiegazione. Prendetevi il tempo che vi serve.

> **Come NON usare la lista**: Seguire ogni punto senza capirne il motivo. Chiudercisi in maniera compulsiva finché non si ha finito. Saltare tra i livelli.

*Attenzione: la lista non è ultimata e chiunque è libero di contribuire come spiegato [QUI](https://gitlab.com/etica-digitale/privasi/blob/master/FAQ.md#come-posso-contribuire-alla-documentazione). I lavori procedono regolarmente, e [QUI](https://gitlab.com/etica-digitale/privasi/commits/master) trovate il registro di tutte le modifiche.*
 
---

## Livello 0: Introduzione
* [Introduzione: cosa me ne importa della privacy?](../percorso/l0-0___intro/)
* [Formiche in una teca](../percorso/l0-1___ants/)
---
## Livello 1: Limitare i danni
* [Rimozione del consenso](../percorso/l1-0___activity-deletion/)
* [Guida a una buona password](../percorso/l1-1___passwords/)
* [Conversazioni, il primo passo: ~~Messenger~~ Telegram](../percorso/l1-2___telegram/)
* [Conversazioni sensibili pt.1 - TV intelligenti e assistenti vocali (Internet delle Cose)](../percorso/l1-3___iot/)
* [Conversazioni sensibili pt.2 - Signal incontra WhatsApp](../percorso/l1-4___signal/)
* [Motori di ricerca: il mondo oltre Google](../percorso/l1-5___search-engine/)
* [~~Microsoft Office~~ Il nostro ufficio digitale (con LibreOffice)](../percorso/l1-6___libreoffice/)
* [Gesti Quotidiani](../percorso/l1-7___daily-habits/)
* [Fuga dal superfluo](../percorso/l1-8___accounts-deletion/)


---
## 🆕 Livello 2: Mezzi per muoversi

* [Cookie: briciole di internet](../percorso/l2-0___cookies/)
* [Non nel mio nome pt.1: F-Droid, il negozio di app trasparente](../percorso/l2-1___f-droid/)
* [Non nel mio nome pt.2: ~~Chrome~~ i browser, le finestre sulla rete](../percorso/l2-2___firefox/)
* [Posta elettronica usa e getta](../percorso/l2-3___10minute/)
* [~~Youtube~~ Video in un riflesso](../percorso/l2-4___youtube/)
* [~~Google Maps~~ Mappe libere](../percorso/l2-5___osm/)
* [Bloccare i traccianti di terze parti](../percorso/l2-6___nextdns/)
* [Esodo](../percorso/l2-7___exodus/)


---
## Livello 3: Compartimentare
> [Seguici su Telegram](https://t.me/eticadigitale) per sapere l'uscita del prossimo livello
